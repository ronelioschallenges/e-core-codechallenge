# Ronélio's E-core challenge

This project was generated to achieve the coding challenge. The purpose of the challenge was generate an endpoint that:

- Creates roles
- Assign a role to a team member
- Look up a role for a membership (based on User ID and Team ID)
- Look up memberships for a role

The chart below explains the relationships expected:
![](/docs/prints/entities.png/)

### How this challenge was achieved

The main problem of this challenge was focused on the Role persistence. To do that, I've created the expected endpoints and also some services (UserServices, UserRoleTeamServices) as helpers. This project uses MVC architecture and it is organized by Model, Service, Repository and Controller, for example:

- role (package)
    - Role
    - RoleController
    - RoleRepository
    - RoleService

![](/docs/prints/codestructure.png/)

The challenge was not clear about it should store teams and users, though that I decided to cache the users when it does not exists on local cache. It could be an improvement expanded it to TeamService.

# Requirements
- Java 10+
- Maven
- IntelliJ (****recommended****)

# Third-party

To compose this project, some libraries and frameworks were used for support:

- JUnit [see its doc](https://junit.org/junit5/docs/current/user-guide/)
- Maven [see its doc](https://spring.io/quickstart)
- Spring Boot [see its doc](https://spring.io/quickstart)

## Running the API

You can run this example by running on terminal the following command:

Add **-DskipTests** if you want to skip tests.

    mvn spring-boot:run -DskipTests

The command above will start ****Spring Boot Application****, and then you can call any API available. You can use ****Postman**** for tests purposes or use the **Swagger UI** available at http://localhost:8080/swagger-ui/index.html

## Endpoints

![](/docs/prints/endpoints.png/)