package com.ecore.ronelio.user;

import com.ecore.ronelio.constants.Consts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class UserService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User save(User User) {
        return userRepository.save(User);
    }

    public User update(User User) {
        return userRepository.save(User);
    }

    public boolean existsById(UUID id) {
        return userRepository.existsById(id);
    }

    public Optional<User> findByIdAndCache(UUID id) throws Exception {
        var optUser = userRepository.findById(id);

        if(optUser.isEmpty()) {
            var webUser = this.restTemplate.getForEntity(Consts.GATEWAY_ENDPOINT + "users/" + id.toString(), User.class);
            if(webUser.getBody() == null) {
                throw new Exception("User not found on the webservice!");
            }
            return Optional.of(this.save(webUser.getBody()));
        }
        return optUser;
    }

    public void delete(UUID id) {
        userRepository.deleteById(id);
    }
}
