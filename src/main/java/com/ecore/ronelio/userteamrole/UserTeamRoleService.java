package com.ecore.ronelio.userteamrole;

import com.ecore.ronelio.user.User;
import com.ecore.ronelio.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.UUID;

@Service
@Transactional
public class UserTeamRoleService {

    @Autowired
    private UserTeamRoleRepository userTeamRoleRepository;

    @Autowired
    private UserService userService;

    public UserTeamRole save(UserTeamRole userTeamRole) {
        return userTeamRoleRepository.save(userTeamRole);
    }

    public UserTeamRole findByTeamIdAndUserId(UUID teamId, UUID userId) {
        var result = userTeamRoleRepository.findByTeamIdAndUserId(teamId, userId);
        return result.orElse(null);
    }

    public Collection<User> getMemberShipsByRole(UUID roleId) {
        return userTeamRoleRepository.findMembershipsByRole(roleId);
    }

}
