package com.ecore.ronelio.userteamrole;

import com.ecore.ronelio.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserTeamRoleRepository extends JpaRepository<UserTeamRole, UUID> {

    Optional<UserTeamRole> findByTeamIdAndUserId(UUID teamId, UUID userId);

    @Query(
            value = "select u from User u join UserTeamRole ut on ut.userId = u.id where ut.role.id = :roleId ")
    Collection<User> findMembershipsByRole(UUID roleId);

}