package com.ecore.ronelio.role;

import com.ecore.ronelio.user.UserService;
import com.ecore.ronelio.userteamrole.UserTeamRole;
import com.ecore.ronelio.userteamrole.UserTeamRoleDTO;
import com.ecore.ronelio.userteamrole.UserTeamRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    UserService userService;

    @Autowired
    UserTeamRoleService userTeamRoleService;

    @Transactional(readOnly = true)
    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    public Role save(Role role) {
        return roleRepository.save(role);
    }

    public Role update(Role role) {
        return roleRepository.save(role);
    }

    public boolean existsById(UUID id) {
        return roleRepository.existsById(id);
    }

    public Optional<Role> findById(UUID id) {
        return roleRepository.findById(id);
    }

    public void delete(UUID id) {
        roleRepository.deleteById(id);
    }

    public UserTeamRole linkRoleToUser(UserTeamRoleDTO userTeamRoleDTO) throws Exception {

        UserTeamRole userTeamRole = new UserTeamRole();

        //Check if the role exists
        if(roleRepository.findById(userTeamRoleDTO.getRoleId()).isPresent()) {
            //cache the use
            var user = userService.findByIdAndCache(userTeamRoleDTO.getUserId());

            if(user.isPresent()) {

                userTeamRole.setTeamId(userTeamRoleDTO.getTeamId());
                userTeamRole.setUserId(userTeamRoleDTO.getUserId());
                userTeamRole.setRole(new Role(userTeamRoleDTO.getRoleId()));
                userTeamRoleService.save(userTeamRole);
            } else {
                throw  new Exception("User not found!");
            }

        } else {
            throw  new Exception("Role not found!");
        }

        return userTeamRole;
    }

    public Role findByTeamAndUserId(UUID teamId, UUID userId) {
         var userTeamRole =  userTeamRoleService.findByTeamIdAndUserId(teamId, userId);
        if(userTeamRole != null) {
            return userTeamRole.getRole();
        }
        return null;
    }
}
