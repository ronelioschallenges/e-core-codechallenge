package com.ecore.ronelio.role;

import com.ecore.ronelio.user.User;
import com.ecore.ronelio.userteamrole.UserTeamRole;
import com.ecore.ronelio.userteamrole.UserTeamRoleDTO;
import com.ecore.ronelio.userteamrole.UserTeamRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Api(tags = "Roles", value = "Endpoint for rules")
@RestController
@RequestMapping("/api")
public class RoleController {

    @Autowired
    RoleService roleService;

    @Autowired
    UserTeamRoleService userTeamRoleService;

    @ApiOperation(value = "Returns a list of roles")
    @ApiResponses(value = {
            @ApiResponse(
                    code = 200, message = "List of roles"
            ),
            @ApiResponse(
                    code = 500, message = "Unexpected error on server"
            )
    })
    @GetMapping(value = "/roles", produces = "application/json")
    public List<Role> getRoles() {
        return roleService.findAll();
    }

    @ApiOperation(value = "Create a new Role")
    @ApiResponses(value = {
            @ApiResponse(
                    code = 201, message = "Role created successfully"
            ),
            @ApiResponse(
                    code = 500, message = "Unexpected error on server"
            )
    })
    @PostMapping(path = "/role", produces = "application/json")
    public ResponseEntity<Role> createRole(@Valid @RequestBody Role role)  throws URISyntaxException {
        var result = roleService.save(role);
        return ResponseEntity
                .created(new URI("/api/role" + result.getId()))
                .body(result);
    }

    @ApiOperation(value = "Update a Role")
    @ApiResponses(value = {
            @ApiResponse(
                    code = 204, message = "No content when the Role is not found"
            ),
            @ApiResponse(
                    code = 200, message = "Role updated successfully"
            ),
            @ApiResponse(
                    code = 400, message = "Invalid request"
            )
    })
    @PutMapping(path = "/role/{id}", produces = "application/json")
    public ResponseEntity<Role> updateRole(
            @PathVariable(value = "id", required = false) final UUID id,
            @Valid @RequestBody Role role) {

        if(id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if(!roleService.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        role.setId(id);
        var result = roleService.update(role);
        return ResponseEntity
                .ok()
                .body(result);
    }

    @ApiOperation(value = "Delete a Role")
    @ApiResponses(value = {
            @ApiResponse(
                    code = 204, message = "Role deleted successfully"
            ),
            @ApiResponse(
                    code = 400, message = "Invalid request"
            )
    })
    @DeleteMapping(path = "/role/{id}")
    public ResponseEntity<Role> deleteRole(
            @PathVariable(value = "id", required = false) final UUID id) {

        if(id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if(!roleService.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        roleService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @ApiOperation(value = "Assigns a role to a team member")
    @ApiResponses(value = {
            @ApiResponse(
                    code = 201, message = "Role associated successfully"
            ),
            @ApiResponse(
                    code = 500, message = "Unexpected error on server"
            )
    })
    @PostMapping(path = "/role/linkuser", produces = "application/json")
    public ResponseEntity<UserTeamRole> linkRoleToUserAndTeam(@Valid @RequestBody UserTeamRoleDTO userTeamRoleDTO) throws Exception {

        var result = roleService.linkRoleToUser(userTeamRoleDTO);
        return ResponseEntity
                .created(new URI("/api/role/" + result.getRole().getId()))
                .body(result);
    }

    @ApiOperation(value = "Returns a role associated to user and team")
    @ApiResponses(value = {
            @ApiResponse(
                    code = 200, message = "Role"
            ),
            @ApiResponse(
                    code = 500, message = "Unexpected error on server"
            )
    })
    @GetMapping(value = "/rolebyuserteam/{user}/{team}", produces = "application/json")
    public ResponseEntity<Role> getRoleByTeamAndUserID(
            @PathVariable(value = "user") final UUID userId,
            @PathVariable(value = "team") final UUID teamId
    ) throws URISyntaxException {
        var result = roleService.findByTeamAndUserId(teamId, userId);
        return ResponseEntity
                .ok()
                .body(result);
    }

    @ApiOperation(value = "Returns a list of memberships of a role")
    @ApiResponses(value = {
            @ApiResponse(
                    code = 200, message = "List of Memberships"
            ),
            @ApiResponse(
                    code = 500, message = "Unexpected error on server"
            )
    })
    @GetMapping(value = "/role/{roleId}/memberships", produces = "application/json")
    public ResponseEntity<Collection<User>> getMemberShipsByRole(
            @PathVariable(value = "roleId") final UUID roleId
    ) {
        var result = userTeamRoleService.getMemberShipsByRole(roleId);
        return ResponseEntity
                .ok()
                .body(result);
    }

}
