package com.ecore.ronelio.role;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue
    @ApiModelProperty(value = "Role UUID")
    @Column(name = "id", nullable = false)
    private UUID id;

    @ApiModelProperty(value = "Role Name")
    @Column(name = "role_name")
    private String roleName;

    public Role(){};
    public Role(UUID id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}