package com.ecore.ronelio.role;

import com.ecore.ronelio.userteamrole.UserTeamRole;
import com.ecore.ronelio.userteamrole.UserTeamRoleService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class RoleServiceTest {

    @Autowired
    private RoleService roleServiceTest;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private UserTeamRoleService userTeamRoleService;

    @Test
    void findAll() {
        when(roleRepository.findAll()).thenReturn(List.of(new Role(), new Role()));
        var result = roleServiceTest.findAll();
        assertEquals(2, result.size());
    }

    @Test
    void shouldSaveRole() {
        var role = new Role();
        role.setId(UUID.randomUUID());
        role.setRoleName("Role Name Test");
        when(roleRepository.save(role)).thenReturn(role);
        assertEquals(role, roleServiceTest.save(role));
    }

    @Test
    void existsById() {
        var uuid = UUID.randomUUID();
        when(roleRepository.existsById(uuid)).thenReturn(true);
        assertTrue(roleServiceTest.existsById(uuid));
    }

    @Test
    void shouldDeleteRole() {
        var uuid = UUID.randomUUID();
        roleServiceTest.delete(uuid);
        verify(roleRepository, times(1)).deleteById(uuid);
    }

    @Test
    void shouldFindByTeamAndUserId() {

        UUID roleId = UUID.randomUUID();
        UUID teamId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();

        var role = new Role();
        role.setId(roleId);
        role.setRoleName("Test Role Name");

        var userTeamRole = new UserTeamRole();
        userTeamRole.setTeamId(teamId);
        userTeamRole.setUserId(userId);
        userTeamRole.setRole(role);

        when(userTeamRoleService.findByTeamIdAndUserId(UUID.randomUUID(), UUID.randomUUID())).thenReturn(userTeamRole);
        assertEquals(userTeamRole.getRole(), role);
    }

    @Test
    void shouldNotFindByTeamAndUserId() {
        var role = roleServiceTest.findByTeamAndUserId(UUID.randomUUID(), UUID.randomUUID());
        assertNull(role);
    }
}