package com.ecore.ronelio.role;

import com.ecore.ronelio.user.User;
import com.ecore.ronelio.userteamrole.UserTeamRole;
import com.ecore.ronelio.userteamrole.UserTeamRoleDTO;
import com.ecore.ronelio.userteamrole.UserTeamRoleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.WebApplicationContext;

import java.net.URISyntaxException;
import java.util.*;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class RoleControllerTest {


    @MockBean
    private UserTeamRoleService userTeamRoleService;

    @MockBean
    private RoleService roleService;

    @MockBean
    private RoleRepository roleRepository;

    @InjectMocks
    private RoleController roleController;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper;

    AutoCloseable autoCloseable;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(roleController).build();
        objectMapper = new ObjectMapper();
        autoCloseable = MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldReturnListOfRoles() throws Exception {

        Role role1 = new Role();
        role1.setId(UUID.randomUUID());
        role1.setRoleName("Test Role Controller");

        Role role2 = new Role();
        role2.setId(UUID.randomUUID());
        role2.setRoleName("Test Role Controller");

        List<Role> roles = new ArrayList<>(
                Arrays.asList(role1, role2));
        when(roleService.findAll()).thenReturn(roles);
        mockMvc.perform(get("/api/roles"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(roles.size()))
                .andDo(print());
    }

    @Test
    void shouldCreateRole() throws Exception {
        Role role = new Role();

        when(roleService.save(any())).thenReturn(role);
        mockMvc.perform(post("/api/role").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(role)))
                .andExpect(status().isCreated())
                .andDo(print());
    }

    @Test
    void shouldUpdateRole() throws Exception {
        UUID id = UUID.randomUUID();
        Role role = new Role();
        role.setId(id);
        role.setRoleName("Role test name");

        when(roleService.existsById(id)).thenReturn(true);
        when(roleService.update(any())).thenReturn(role);

        mockMvc.perform(put("/api/role/{id}", id).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(role)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.id").value(role.getId().toString()))
                .andExpect(jsonPath("$.roleName").value(role.getRoleName()));

    }

    @Test
    void shouldDeleteRole() throws Exception {
        UUID id = UUID.randomUUID();

        when(roleService.existsById(id)).thenReturn(true);

        mockMvc.perform(delete("/api/role/{id}", id))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    @Test
    void shouldLinkUsersToRole() throws Exception {

        UUID roleId = UUID.randomUUID();
        UUID teamId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();

        UserTeamRoleDTO userTeamRoleDTO = new UserTeamRoleDTO();
        userTeamRoleDTO.setRoleId(roleId);
        userTeamRoleDTO.setTeamId(teamId);
        userTeamRoleDTO.setUserId(userId);

        UserTeamRole userTeamRole = new UserTeamRole();
        Role role = new Role();
        role.setId(roleId);
        role.setRoleName("Test Role Controller");
        userTeamRole.setRole(role);
        userTeamRole.setUserId(userId);
        userTeamRole.setTeamId(teamId);

        when(roleService.linkRoleToUser(any())).thenReturn(userTeamRole);

        mockMvc.perform(post("/api/role/linkuser").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userTeamRoleDTO)))
                .andExpect(status().isCreated())
                .andDo(print())
                .andExpect(jsonPath("$.role.id").value(userTeamRoleDTO.getRoleId().toString()))
                .andExpect(jsonPath("$.teamId").value(userTeamRoleDTO.getTeamId().toString()))
                .andExpect(jsonPath("$.userId").value(userTeamRoleDTO.getUserId().toString()));

    }

    @Test
    void shouldReturnRoleByTeamAndUserID() throws Exception {

        UUID roleId = UUID.randomUUID();
        UUID teamId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();

        Role role = new Role();
        role.setId(roleId);
        role.setRoleName("Test Role Controller");

        when(roleService.findByTeamAndUserId(teamId, userId)).thenReturn(role);

        mockMvc.perform(get("/api/rolebyuserteam/{user}/{team}", userId, teamId).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(role)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.id").value(roleId.toString()));
    }

    @Test
    void shouldReturnMemberShipsByRole() throws Exception {

        UUID roleId = UUID.randomUUID();

        Collection<User> users = List.of(new User(), new User(), new User());

        when(userTeamRoleService.getMemberShipsByRole(roleId)).thenReturn(users);

        mockMvc.perform(get("/api/role/{roleId}/memberships", roleId))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.size()").value(3));
    }
}